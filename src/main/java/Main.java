import domain.*;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        MyApplication app = new MyApplication();
        app.start();
    }
}
