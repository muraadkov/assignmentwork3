package domain;
public class Password {
    private String passwordStr;

    public Password(String passwordStr){
        setPasswordStr(passwordStr);
    }

    public void setPasswordStr(String passwordStr) {
        if (checkPassword(passwordStr)) {
            this.passwordStr = passwordStr;
        } else {
            System.out.println("Incorrect password! Try again");
            return;
        }
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public static boolean checkPassword(String passwordStr) {
        boolean lowerCase = false, upperCase = false, symbolCase = false;
        if (passwordStr.length() < 8) return false;
        for(int i = 0 ; i < passwordStr.length(); i++){
            if(Character.isLowerCase(passwordStr.charAt(i))) lowerCase = true;
             else if(Character.isUpperCase(passwordStr.charAt(i))) upperCase = true;
             else if(passwordStr.charAt(i) >= 33 && passwordStr.charAt(i) <= 47) symbolCase = true;
        }
        if(!(lowerCase && upperCase && symbolCase)) return false;
        return true;
    }

}