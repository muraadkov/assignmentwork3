package domain;

public class Email {
    private String emailStr;

    public Email(String emailStr) {
        setEmailStr(emailStr);
    }

    public String getEmailStr() {
        return emailStr;
    }

    public void setEmailStr(String emailStr) {
        if (checkEmailFormat(emailStr)) {
            this.emailStr = emailStr;
        } else {
            System.out.println("Email.setEmailStr : Incorrect email format : " + emailStr);
            return;
        }
    }

    private static boolean checkEmailFormat(String email) {
        int pos = email.indexOf('@');
        if (pos < 1) return false;

        String domain = email.substring(pos + 1); // abai@mail.com
        pos = domain.indexOf('.');
        int len = domain.length();
        if (pos < 1 || pos == domain.length() - 1) return false;
        return true;
    }
}
