package domain;

import java.time.LocalDate;

public class Message {
    private int id;
    private User sender;
    private String content;
    private String date;

    public Message(){
        sender = null;
        content = "";
        date = LocalDate.now().toString();
    }

    public Message(User sender, String content, String date){
        setSender(sender);
        setContent(content);
        setDate(date);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getSender() {
        return sender;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
