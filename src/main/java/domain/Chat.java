package domain;

import java.util.Vector;

public class Chat extends Message{
    private int id;
    private User createdBy;
    private Vector<Message> messages = new Vector<Message>();
    private Vector<User> participants = new Vector<User>();
    private static int id_gen = 0;

    public Chat(){
        super();
    }

    public Chat(User sender, String content, String date, User createdBy,Message message, User participant){
        super(sender, content, date);
        generateId();
        setMessages(message);
        setCreatedBy(createdBy);
        setParticipants(participant);
    }

    public void generateId(){
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setParticipants(User participant) {
        participants.add(participant);
    }

    public User getParticipants() {
        if(!participants.isEmpty()) {
            for (int i = 0; i < participants.size(); i++) {
                return participants.get(i);
            }
        }
        return null;

    }

    public void setMessages(Message message) {
        messages.add(message);
    }

    public void getMessages() {
        if(!messages.isEmpty()) {
            for (int i = 0; i < messages.size(); i++) {
                System.out.println(messages.get(i).getSender().getUserName() + ": " + messages.get(i).getContent());
            }
        }else System.out.println("You haven`t any message");
    }
}
