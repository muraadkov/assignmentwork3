package domain;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Array;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

public class MyApplication {
    private Scanner sc = new Scanner(System.in);
    private Vector<User> users;
    private int cnt = 0;
    private User signedUser;
    private Vector<Chat> chats;

    public MyApplication() {
        users = new Vector<User>();
        chats = new Vector<Chat>();
        signedUser = null;
    }

    private void addUsers() throws IOException {
        File file = new File("C:\\Users\\murka\\IdeaProjects\\secondLesson.2\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);

        while (fileScanner.hasNext()) {
            int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0;
            int id2 = fileScanner.nextInt();
            String info = fileScanner.nextLine();
            for (int i = 0; i < info.length(); i++) {
                if (info.charAt(i) == ' ') {
                    if (cnt1 > 0 && cnt2 > 0 && cnt3 > 0 && cnt4 > 0 && cnt5 == 0) cnt5 = i;
                    if (cnt1 > 0 && cnt2 > 0 && cnt3 > 0 && cnt4 == 0) cnt4 = i;
                    if (cnt1 > 0 && cnt2 > 0 && cnt3 == 0) cnt3 = i;
                    if (cnt1 > 0 && cnt2 == 0) cnt2 = i;
                    if (cnt1 == 0) cnt1 = i;
                }
            }
            String name2 = info.substring(1, cnt1);
            String surname2 = info.substring(cnt1 + 1, cnt2);
            String userName2 = info.substring(cnt2 + 1, cnt3);
            String pass2 = info.substring(cnt3 + 1, info.length());
            Password password2 = new Password(pass2);
            users.add(new User(id2, name2, surname2, userName2, password2));
        }
    }
    private void saveUsers() throws IOException {
        FileWriter fileWriterr = new FileWriter("C:\\Users\\murka\\IdeaProjects\\secondLesson.2\\src\\com\\company\\db.txt", false);
        BufferedWriter bw2 = new BufferedWriter(fileWriterr);
        bw2.write("");
        bw2.close();
        for (int i = 0; i < users.size(); i++) {
            int id = users.get(i).getId();
            String name = users.get(i).getName();
            String surname = users.get(i).getSurname();
            String username = users.get(i).getUserName();
            String password = users.get(i).getPassword().getPasswordStr();
            String data = id + " " + name + " " + surname + " " + username + " " + password;
            FileWriter fileWriter = new FileWriter("C:\\Users\\murka\\IdeaProjects\\secondLesson.2\\src\\com\\company\\db.txt", true);
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.write(data);
            bw.newLine();
            bw.close();
        }
    }


    private void addFriend(User user) {
        User friendUser = null;
        for (int i = 0; i < users.size(); i++) {
            System.out.println(users.get(i).getName() + " " + users.get(i).getSurname() + " " + users.get(i).getUserName());
        }
        System.out.println("Write a username to search friend.");
        sc.nextLine();
        String uName = sc.nextLine();
        for (int i = 0; i < users.size(); i++) {
            if (uName.equals(users.get(i).getUserName())) {
                friendUser = new User(users.get(i).getId(), users.get(i).getName(), users.get(i).getSurname(), users.get(i).getUserName(), users.get(i).getPassword());
                user.setFriends(friendUser);
            }
        }
    }
    private void myFriends(User user) {
        user.getFriends();
    }

    private void userProfile(User user) throws IOException {

        System.out.println("What are you want? \n1.Check profile \n2.My friends\n3.Add friend\n4.Message\n5.Settings\n6.Logout");
        int type = sc.nextInt();
        switch (type) {
            case 1:
                System.out.println(user);
                userProfile(user);
                break;
            case 2:
                myFriends(user);
                userProfile(user);
                break;
            case 3:
                addFriend(user);
                userProfile(user);
                break;
            case 4:
                messageMenu(user);
                break;
            case 5:
                settingsMenu(user);
                break;
            case 6:
                logOut();
                break;
            default:
                break;
        }
    }

    private void logOut(){
        signedUser = null;
    }

    private void settingsMenu(User user) throws IOException {
        System.out.println("1.Change name\n2.Change surname\n3.Change email\n4.Change password\n5.Exit");
        int type = sc.nextInt();
        switch (type){
            case 1:
                changeName(user);
                break;
            case 2:
                changeSurname(user);
                break;
            case 3:
                changeEmail(user);
                break;
            case 4:
                changePassword(user);
                break;
            case 5:
                userProfile(user);
                break;
        }
    }
    private void changeName(User user) throws IOException {
        System.out.println("Your last password: ");
        String pass = sc.nextLine();
        if(pass == user.getPassword().getPasswordStr()){
            System.out.println("Your new name: ");
            sc.nextLine();
            String newName = sc.nextLine();
            user.setName(newName);
        }
        else{
            System.out.println("Incorrect password! \n1.Try again\n2.Exit");
            int type = sc.nextInt();
            switch(type){
                case 1:
                    changeName(user);
                    break;
                case 2:
                    settingsMenu(user);
                    break;
            }
        }
    }
    private void changeSurname(User user) throws IOException {
        System.out.println("Your last password: ");
        String pass = sc.nextLine();
        if(pass == user.getPassword().getPasswordStr()){
            System.out.println("Your new name: ");
            sc.nextLine();
            String newSurname = sc.nextLine();
            user.setSurname(newSurname);
        }
        else{
            System.out.println("Incorrect password! \n1.Try again\n2.Exit");
            int type = sc.nextInt();
            switch(type){
                case 1:
                    changeSurname(user);
                    break;
                case 2:
                    settingsMenu(user);
                    break;
            }
        }
    }
    private void changeEmail(User user) throws IOException {
        System.out.println("Your last password: ");
        String pass = sc.nextLine();
        if(pass == user.getPassword().getPasswordStr()){
            System.out.println("Your new email: ");
            sc.nextLine();
            String newEmail = sc.nextLine();
            Email email = new Email(newEmail);
            user.setEmail(email);
        }
        else{
            System.out.println("Incorrect password! \n1.Try again\n2.Exit");
            int type = sc.nextInt();
            switch(type){
                case 1:
                    changeEmail(user);
                    break;
                case 2:
                    settingsMenu(user);
                    break;
            }
        }
    }
    private void changePassword(User user) throws IOException {
        System.out.println("Your last password: ");
        String pass = sc.nextLine();
        if(pass == user.getPassword().getPasswordStr()){
            System.out.println("Your new password: ");
            sc.nextLine();
            String newPassword = sc.nextLine();
            Password password = new Password(newPassword);
            user.setPassword(password);
        }
        else{
            System.out.println("Incorrect password! \n1.Try again\n2.Exit");
            int type = sc.nextInt();
            switch(type){
                case 1:
                    changePassword(user);
                    break;
                case 2:
                    settingsMenu(user);
                    break;
            }
        }
    }

    private void authentication() throws IOException {
        while (true) {
            System.out.println("1.Sign up\n2.Sign in\n3.Exit");
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    signUp();
                    break;
                case 2:
                    signIn();
                    break;
                case 3:
                    menu();
                    break;

            }
        }
    }
    private void signIn() throws IOException {
        boolean check = false;
        String name = "";
        int id = 0;
        String surname = "";
        String email2 = "";
        System.out.println("Your username: ");
        sc.nextLine();
        String userName = sc.nextLine();
        System.out.println("Your password: ");
        String pass = sc.nextLine();
        Password password = new Password(pass);
        for (int i = 0; i < users.size(); i++) {
            System.out.println(users.get(i).getUserName());
            if (users.get(i).getUserName().equals(userName) && users.get(i).getPassword().getPasswordStr().equals(pass)) {
                check = true;
                id = users.get(i).getId();
                name = users.get(i).getName();
                surname = users.get(i).getSurname();
                break;

            } else {
                continue;
            }

        }
        Email email = new Email(email2);
        if (check) {
            signedUser = new User(id,name, surname, userName, password);
            userProfile(signedUser);
        } else {
            System.out.println("Maybe you didn`t register? Go to authentication!");
            authentication();
        }
    }
    private void signUp() throws IOException {
        boolean existUsername = false;
        File file = new File("C:\\Users\\murka\\IdeaProjects\\secondLesson.2\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);
        System.out.println("Your id: ");
        int id = sc.nextInt();
        sc.nextLine();
        System.out.println("Your name: ");
        String name = sc.nextLine();
        System.out.println("Your surname: ");
        String surname = sc.nextLine();
        System.out.println("Your username: ");
        String userName = sc.nextLine();
        for(int i = 0 ; i < users.size(); i++){
            if(users.get(i).getUserName().equals(userName)){
                existUsername = true;
                break;
            }
        }
        System.out.println("Your email: ");
        String email2 = sc.nextLine();
        Email email = new Email(email2);
        System.out.println("Your password: ");
        String pass = sc.nextLine();
        Password password = new Password(pass);
        if(existUsername) {
            User user = new User(id, name, surname, userName, email, password);
            users.add(user);
            System.out.println("Enter -1, back to menu");
            int num = sc.nextInt();
            if (num == -1) authentication();
        }else {
            System.out.println("A user with the same username already exists");
            signUp();
        }
    }


    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication.\n2.Exit");
                int choice = sc.nextInt();
                switch (choice) {
                    case 1:
                        authentication();
                        break;
                    case 2:
                        start();
                        break;
                }
            } else {
                userProfile(signedUser);
            }
        }
        //if a user is not signed id call authentication
    }
    public void start() throws IOException {
        addUsers();
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command: ");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    menu();
                    break;
                case 2:
                    saveUsers();
                    System.exit(0);
                default:
            }

        }
    }

    public void messageMenu(User user) throws IOException {
        System.out.println("1.Send message\n2.Open chats\n3.Exit");
        int number = sc.nextInt();
        switch(number){
            case 1:
                sendMessage(user);
                userProfile(user);
                break;
            case 2:
                openTheChats(user);
                userProfile(user);
                break;
            case 3:
                userProfile(user);
                break;
        }
    }
    public void sendMessage(User user) throws IOException {
        User chatUser = null; boolean existChat = false;
        int ind = 0; int id = 0;
        System.out.println("To whom you want to write?");
        user.getFriends();
        System.out.println("Write an username of friend");
        sc.nextLine();
        String userName = sc.nextLine();
        System.out.println("Write the message: ");
        String content = sc.nextLine();
        for(int i = 0 ; i < users.size(); i++){
            if(users.get(i).getUserName().equals(userName)){
                chatUser = users.get(i);
            }
        }

        Message message = new Message(user, content,LocalDate.now().toString());
        if(chats.size() == 0){
            chats.add(new Chat(message.getSender(),message.getContent(),message.getDate(),user,message, chatUser));
        }
        else {
            for (int i = 0; i < chats.size(); i++) {
                if (chats.get(i).getCreatedBy().equals(user) && chats.get(i).getParticipants().equals(chatUser)) {
                    existChat = true;
                    ind = i;
                    break;
                }
            }
            if (existChat) {
                chats.get(ind).setMessages(message);
            } else {
                chats.add(new Chat(message.getSender(), message.getContent(), message.getDate(), user, message, chatUser));
            }
        }
        FileWriter fileWriter = new FileWriter("C:\\Users\\murka\\IdeaProjects\\secondLesson.2\\src\\com\\company\\messages.txt", true);
        String data = user.getId() + " " + "#" + content + "#" + id;
        fileWriter.write(data);
    }
    public void openTheChats(User user) throws IOException {
        for(int i = 0 ; i < chats.size() ; i++){
            System.out.println(chats.get(i).getId() + ". created by: " + chats.get(i).getCreatedBy().getUserName() + "\nparticipants: " + chats.get(i).getParticipants().getUserName());
        }
        System.out.println("Which chat you want to open?");

        int id = sc.nextInt(); int ind = 0;
        for(int i = 0 ; i < chats.size(); i++){
            if(chats.get(i).getId() == id){
                ind = i;
            }
        }
        chats.get(ind).getMessages();
        System.out.println("Do you want to answer? \n1. Yes \n2. No");
        int answer = sc.nextInt();
        switch(answer){
            case 1:
                Message message = null;
                System.out.println("Write the message: ");
                sc.nextLine();
                String content = sc.nextLine();
                message = new Message(user, content, LocalDate.now().toString());
                chats.get(ind).setMessages(message);

            case 2:
                System.out.println("1.Add member\n2.Open the another chat\n3.Exit");
                int number = sc.nextInt();
                switch(number){
                    case 1:
                        System.out.println("To whom you want to write?");
                        user.getFriends();
                        System.out.println("Write an username of friend");
                        sc.nextLine();
                        String userName = sc.nextLine();
                        for(int i = 0; i < users.size(); i++){
                            if(users.get(i).getUserName().equals(userName)){
                                chats.get(ind).setParticipants(users.get(i));
                            }else{
                                continue;
                            }
                        }
                        break;
                    case 2:
                        openTheChats(user);
                        break;
                    case 3:
                        messageMenu(user);
                        break;
                }
                break;
        }

    }
}
