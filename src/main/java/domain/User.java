package domain;

import java.util.HashSet;
import java.util.Vector;

public class User {
    private int id;
    private String userName;
    private HashSet<User> friends = new HashSet<User>();
    private String name;
    private String surname;
    private Password password;
    private Email email = new Email("default@gmail.com");
    private static int id_gen = 0;


    public User(int id2, String name, String surname, String userName, Password password){
        setName(name);
        setSurname(surname);
        setUserName(userName);
        setPassword(password);
    }

    public User(int id, String name, String surname, String userName, Email email, Password password){

        this(id, name, surname, userName, password);
        setEmail(email);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFriends(User friend) {
        friends.add(friend);
    }

    public void getFriends() {
        if(!friends.isEmpty()) {
            System.out.println("Your friends: ");
            for(User u : friends){
                System.out.println(u.getId()+ " " + u.getName() + " " + u.getSurname() + " " + u.getUserName());
            }
        }
        else{
            System.out.println("You haven`t any friend");
        }
    }

    public void generateId(){
        id = id_gen++;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public Email getEmail() {
        return email;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Password getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Hello user " +
                name +
                "\nuserName='" + userName + '\'' +
                "\nid='" + id + '\'' +
                "\nsurname='" + surname + '\'' +
                "\npassword=" + password.getPasswordStr() +
                "\nemail=" + email.getEmailStr();
    }
}
